//CONFIRMAÇÃO DE DELEÇÃO DE POST
function confirmacao(id) {
    Swal.fire({
    title: 'Confirmação:',
    text: "Deseja realmente apagar?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sim, Apagar post!'
    }).then((result) => {
        if (result.value) {
            $id = id;
            Swal.fire(
            'Apagado!',
            'Este post foi apagado.',
            'success'
            )
            window.location.href="/home/apagar/"+id;
        }    
    })
}

//JS da página de Editais
function detailEdital(id){
    window.location.href = "/DetailEdital/"+id;
}
function enviarPesquisa(){
    document.forms['formSearch'].submit();
}
function editalRemove(id){
    Swal.fire({
        title: 'Deseja mesmo excluir?',
        text: "Realmente deseja remover o edital",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, remover!'
      }).then((result) => {
        if (result.value) {
          window.location.href = "/RemoveEdital/"+id;
        }
      })
}

//PRELOADER 
