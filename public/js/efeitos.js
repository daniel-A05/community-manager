window.onload = function(){
    document.getElementById('geradordesenha').click();
};
function ApenasLetras(e) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else {
            return true;
        }
        if (
            (charCode === 32)
            ||
            (charCode > 64 && charCode  < 91) || 
            (charCode > 96 && charCode < 123) ||
            (charCode > 191 && charCode <= 255) // letras com acentos
        ){
            return true;
        } else {
            return false;
        }
    } catch (err) {
        alert(err.Description);
    }
}
function mascaraData( campo, e ){
	var kC = (document.all) ? event.keyCode : e.keyCode;
	var data = campo.value;
	
	if( kC!=8 && kC!=46 )
	{
		if( data.length==2 )
		{
			campo.value = data += '/';
		}
		else if( data.length==5 )
		{
			campo.value = data += '/';
		}
		else
			campo.value = data;
	}
}
     // funçãoo dec2hex: converte numeros decimais em numeros hexadecimais 
     function dec2hex(numero) {
        var base = 16;
        var digito = new Array();
        var i = 0;

        while (numero != 0) {
          i++;
          digito[i] = numero % base;
          numero = Math.floor(numero / base);
        }
        value = "";
        while (i >= 1)  {
          switch (digito[i]) {
            case 10: { value += "0"; break }
            case 11: { value += "1"; break }
            case 12: { value += "2"; break }
            case 13: { value += "3"; break }
            case 14: { value += "4"; break }
            case 15: { value += "5"; break }
            default: { value += digito[i]; break }
          }
          i--;
        }
        return value;
      }
      function GerarSenha() {
        document.forms[0].password.value = "";
        tamanho = document.forms[0].digitos.value;
        // validar o campo *d�gitos*
        if (tamanho < 1 || isNaN(tamanho)) {
          document.forms[0].digitos.focus();
          document.forms[0].digitos.select();
          return;
        }
       // c�digos ASCII decimais
        min = 2;
        max = 5;
        for (i = 1; i <= tamanho; i++) {
          caracter = min + Math.floor((Math.random() * (max * min)));  // 32 a 126
          caracter =  dec2hex(caracter);
          document.forms[0].password.value += caracter;
          document.getElementById("aguarde").innerHTML = "aguarde...";
        }
        document.getElementById("aguarde").innerHTML = "";
      }
    
   
