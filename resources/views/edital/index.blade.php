<!-- index.blade.php - edital -->
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="uk-margin uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light" data-src="{{ asset('image/edital-logo.jpg')}}" uk-img>
        <a id='arrow-left-edital' href="/home" ><i class="fas fa-arrow-left fa-2x"></i></a>
        <div class="container">
            <h1 class="display-4">Editais fomentos e chamadas</h1>
        </div>
    </div>

    <div>
        <form action="/ShowEdital" class="form-inline my-2 my-lg-0" method='post' name='formSearch'>
            @csrf
            <div class="uk-margin">
                <div class="uk-inline">
                    <a class="uk-form-icon uk-form-icon-flip" onclick='enviarPesquisa();'><i class="fas fa-search"></i></a>
                    <input class="form-control mr-sm-2" type="search" name='search' aria-label="Search" id="search-edital">
                </div>
            </div>
        </form>
    </div>
    <!-- Small modal --> 
    <a style="float:right;" uk-toggle  data-target="#bd-example-modal-sm"><i class="fas fa-file-medical fa-2x" aria-hidden="true"></i></a>
    
    <div uk-modal class="modal fade" id="bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="uk-modal-dialog">
                <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="modal-content">
                <div class="uk-modal-header">
                    <h3 class="modal-title" style="text-align:center;color:gray;font-family:segoe ui;">Adicionar editais</h3>
                </div>
                <div id='form-modal' class="modal-body">
                    <form action="{{route('createEdital')}}" method="POST" enctype="multipart/form-data" >
                        @csrf
                        <input class="uk-input uk-width-0-1 uk-margin-small" type="text" placeholder="Titulo" name="titulo"/>

                        <textarea class="uk-textarea uk-width-0-1 uk-margin-small" placeholder="Descrição" name="descricao" rows="2" style='resize: none;'></textarea>
                        
                        <input class="uk-input uk-width-0-1 uk-margin-small" type="url" placeholder="Link do edital" name="link"/>
                        
                        <div class="uk-margin-small">
                            <div uk-form-custom>
                                <input type="file" name="imagem">
                                <button class="uk-button uk-button-default" style="text-transform:capitalize;background-color:gray;color:#fff;"type="button" tabindex="-1">Selecionar mídia</button>
                            </div>
                        </div>                            
                        <div class="uk-modal-footer uk-text-right">
                            <button class="uk-button uk-button-default uk-modal-close" type="button" >Cancelar</button>
                            <button class="uk-button uk-button-primary"  type="submit" style="background:#4bc7ff;">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div style='margin-top: 60px;' class="container">
        <div class='row'>            
            @foreach($edital as $edi)
                <div onclick='detailEdital({{$edi->id}});' class='card-group edital-card mx-auto'>
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ url('storage/edital/'.$edi->imagem) }}" class="card-img-top" style="width:300px; height:170PX;">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$edi->titulo}}</h5>
                            <small>{{$edi->created_at}}</small>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
@endsection
