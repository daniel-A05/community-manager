@extends('layouts.app')
@section('content')
<div class="container">
    <div class="uk-child-width-1-1@s uk-grid-match uk-grid-small" uk-grid>
        <div>
            <div class="card ">
                <div class="uk-card-media-top">
                    <img src="{{ asset('image/edital-logo.jpg') }}"/>
                </div>
                <div class="uk-card-body">
                    <img class="uk-position-center-left uk-margin-left rounded-circle"  data-src="{{ url('storage/users/'.$total[$id]->image) }}" style="width:150px;height:150px;background-color:white;" uk-img/>
                    <h4 class="card-title" >{{$total[$id]->nomeC}}</h4>
                    <label class="card-text" > {{$total[$id]->funcao}}</label>
                    <a href="#" class="uk-position-center-right btn btn-primary"> 
                        <span uk-icon="comments"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-child-width-1-2" uk-grid>
        <div>
            <div class="uk-child-width-2-2 " uk-grid>
                <div>
                    <div class="card uk-card-body">
                        <b class="text-center"><span uk-icon="world" class="uk-margin-bottom"></span>APRESENTAÇÃO</b>                        
                        <label><span uk-icon="quote-right"></span>{{$total[$id]->sobre}}</label>
                        <label><span uk-icon="calendar" class="uk-margin-right"></span>{{ $total[$id]->dataNasc}}</label>                            
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="card uk-card-default uk-card-body text-center"><b>MINHAS PUBLICAÇÕES</b></div>
        </div>
    </div>
    
</div>
@endsection
