<!DOCTYPE html>
 <html lang="pt-br"> 
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Community Manager</title>
  <link rel="shortcut icon" href="{{asset('image/purplelogo.png')}}" />
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/uikit.css')}}" />
  <link rel="stylesheet" href="{{asset('css/twentytwenty-no-compass.css')}}" />

</head>
  <body style="background-color:#1883fc">
    <!--Navbar-->
    <div class="twentytwenty-container ">
        <nav class="navbar navbar-light bg-transparent" style="z-index: 100;">
            <a class="navbar-brand" href="#">
              <a></a>
              <button class="uk-button uk-button-primary  uk-card-body uk-animation-shake uk-animation-reverse button button2" type="button">
                <span uk-icon="home"></span>
              </button>
            </a>
            <div uk-dropdown class="animation: uk-animation-slide-top-small; duration: 1000">
              <ul class="uk-nav uk-dropdown-nav ">
                <li class="uk-active"><a href="{{ route('login') }}">LOGIN</a></li>
                <!-- <li class="uk-active"><a href="#">CADASTRE-SE</a></li> -->
                <li><a href="#QuemSomos" class="py-3 px-0 px-lg-8 rounded js-scroll-trigger">Quem somos?</a></li>
                <li><a href="#Sobre" class="py-3 px-0 px-lg-8 rounded js-scroll-trigger">Sobre o Biolabs</a></li>
                <li><a href="#QuemUsa" class="py-3 px-0 px-lg-8 rounded js-scroll-trigger">Quem está utilizando?</a></li>
                <li class="uk-nav-divider"></li>
                <li><a href="#Iniciativa" class="py-3 px-0 px-lg-8 rounded js-scroll-trigger">Iniciativas</a></li>
                <!-- <li><a href="#o" class="py-3 px-0 px-lg-8 rounded js-scroll-trigger">Outros</a></li> -->
              </ul>
            </div>
          </nav>
      <img src="{{asset('image/h1.jpg')}}" sizes="(min-width: 650px) 650px, 100vw" style="width: 100vw; height: 100vh;" uk-img />
      <img src="{{asset('image/h2.jpg')}}" sizes="(min-width: 650px) 650px, 100vw" style="width: 100vw; height: 100vh;" uk-img />
      
    </div>

    <div class="container uk-margin-large">
      <div  class="uk-section uk-section-medium uk-section-default uk-margin-large">
        <div id="QuemSomos" class="uk-container">
          <h1  style="text-align:center;">Quem Somos?</h1>
          <div class="uk-child-width-1-2@m uk-grid-small uk-grid-match" uk-grid>
            <div>
              <div class="uk-card uk-card-transparent uk-card-body ">
                <img src="{{asset('image/A.png')}}" style="width: 70%;">
              </div>
            </div>
            <div>
              <div class="uk-card uk-card-transparent uk-card-body">
                <h1 style="color: #1883fc">Somos um HUB de inovação
                  no mercado da saúde.</h1>
                <p > Temos como objetivo ajudar empreendedores a desenvolverem e alavancarem negócios e soluções avançadas com foco na melhoria da qualidade de vida do ser humano. Fazemos isso através de programas de especialização, aceleração de startups, espaço físico de coworking, eventos e iniciativas educacionais.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div id="Sobre" class="uk-section uk-section-medium uk-section-default uk-margin-large">
        <div class="uk-container">
          <h1  style="text-align:center;">Sobre o Gestor Biolabs</h1>
          <div class="uk-child-width-1-2@m uk-grid-small uk-grid-match" uk-grid>
            <div>
              <div class="uk-card uk-card-transparent uk-card-body ">
                <h3 class="uk-card-title "><img src="{{asset('image/reuniao.jpg')}}" style="width: 80%;"></h3>
              </div>
            </div>
            <div>
              <div class="uk-card uk-card-transparent uk-card-body">
                <p class="uk-text-justify" style="font-size: 15px;">O gestor de comunidade funciona como uma rede social corporativa, interna ao ICC Biolabs,
                  que abrigrará conteúdos úteis para membros que compõem o corpo gestor da organização e colaboradores,
                  internos e externos das startups. Possivblitando a interaçãp entra ambas as partes e tornando um ambiente mais dinâmico.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="uk-section uk-section-medium uk-section-default">
        <div class="uk-container">
          <h1 style="text-align: center;" id="QuemUsa">Quem está usando?</h1>
          <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div>
                <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                    <a href="#"><img src="{{asset('image/Health.png')}}"></a>
                </div>
            </div>
            <div>
                <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                  <a href="https://plantaoativo.com/"><img src="{{asset('image/plantao.png')}}"></a>
                </div>
            </div>
            <div>
                <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                    <a href="https://www.taquionlabworks.com/"><img src="{{asset('image/LABWORKS.png')}}"></a>
                </div>
            </div>
            <div>
                <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                    <a href="http://www.taquioninovacao.com.br/"><img src="{{asset('image/Taquion.png')}}" /></a>
                </div>
            </div>
            <div>
              <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                <a href="" class="uk-position-center uk-overlay uk-overlay-default"><img src="{{asset('image/LABPACS.png')}}" /></a>
              </div>
            </div>
            <div>
              <div class="uk-box-shadow-small uk-box-shadow-hover-large uk-padding uk-card uk-card-default uk-card-body">
                <a href=""><img src="{{asset('image/remedio-zap.png')}}"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div id="Iniciativa" class="uk-section uk-section-medium uk-section-default uk-margin-large">
        <div class="uk-container">
          <h1  class='text-center uk-margin-medium-bottom'>Gestor de Comunidade é uma iniciativa de:</h1>
            <div class="uk-grid-match uk-child-width-1-2@m" uk-grid>
              <div>
                <div class="uk-card uk-card-primary uk-card-body">
                    <a href="https://www.icc.org.br/"><img src="{{asset('image/icc-logo.png')}}" class="uk-align-center" style="width:250px;" /></a>
                </div>
                </div>
                <div>
                    <div class="uk-card uk-card-primary uk-card-body" style="float:center">
                        <a href="https://iccbiolabs.com/" class="uk-position-center uk-overlay"><img src="{{asset('image/logo.png')}}" class="uk-align-center" style="width:80%;" /></a>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>  
 
  <!--Footer-->
  <footer style="background: white;">
    <div class="container-fluid text-center text-md-left">
      <div class="row">     
        <!-- <hr class="clearfix w-100 d-md-none pb-3"> -->
        <div class="col-md-3 mb-md-0 mb-3 uk-margin-top uk-margin-large-left">
          <b class="text-uppercase" style="color:#1883fc">Endereço:</b>
          <ul class="list-unstyled">
            <li>
              <label><span uk-icon="location" style="color:#1183fc"> </span> Rua Papi Júnior, 1222 Rodolfo Teófilo -
              5º andar
              </label>
            </li>
          </ul>
        </div>
        
        <div class="col-md-3 mb-md-0 mb-3 uk-margin-top uk-margin-right uk-margin-large-left">
          <b class="text-uppercase" style="color:#1883fc">Fale Conosco:</b>
          <ul class="list-unstyled">
            <li>
              <span style="color:#1183fc" uk-icon="mail"></span> contato@iccbiolabs.com
            </li>
            <li>
              <span style="color:#1183fc" uk-icon="receiver"></span> (85)3031-4764
            </li>
            
            
          </ul>
        </div>

        <div class="col-md-3 mb-md-0 mb-3 uk-margin-top" >
          <b class="text-uppercase" style="color:#1883fc">Siga-nos:</b>
          <p><span style="color:#1183fc" uk-icon="chevron-right"></span>Acompanhe nossas redes sociais </p>
          <ul>  
            <a href="https://www.instagram.com/iccbiolabs/" style="background-color:#1183fc;" class="uk-icon-button text-white uk-margin-small-right" uk-icon="instagram"></a>
            <a href="https://pt-br.facebook.com/iccbiolabs/" style="background-color:#1183fc;" class="uk-icon-button text-white  uk-margin-small-right" uk-icon="facebook"></a>
            <a href="https://br.linkedin.com/company/icc-biolabs" style="background-color:#1183fc;" class="uk-icon-button text-white" uk-icon="linkedin"></a>
          </ul>
        </div>
        
      </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright
      <a href="https://www.icc.org.br/">  </a>
    </div>
  </footer>
  
  
  <script src="{{ asset('js/uikit.js') }}" defer></script>
  <script src="{{ asset('js/uikit-icons.js') }}" defer></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script src="{{ asset('js/jquery.event.move.js') }}"></script>
  <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
  <script src="{{ asset('js/jquery.easing.min.js') }}"></script>

  <!-- SCROLL -->
  <script>
    (function($) {
      "use strict"; // Start of use strict
      // Smooth scrolling using jQuery easing
      $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: (target.offset().top - 70)
            }, 1000, "easeInOutExpo");
            return false;
          }
        }
      });
    })(jQuery);
  </script>
  <!-- FOTOS HOME -->
  <script>
    $(window).load(function(){
      $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.5});
      $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0.5, orientation: 'vertical'});
    });
  </script>
  </body>
</html>