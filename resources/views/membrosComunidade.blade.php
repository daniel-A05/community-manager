@extends('layouts.app')
@section('content')
<div class="container" >
        <div class="uk-margin uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light" data-src="{{ asset('image/edital-logo.jpg')}}" uk-img>
            <a id='arrow-left-edital' href="/home" ><i class="fas fa-arrow-left fa-2x"></i></a>
            <div class="container">
                <h1 class="display-4">Membros da comunidade</h1>
            </div>
        </div>
    <h3 style="text-align:center;color:grey;font-family:arial;font-size:24px;">Startups</h3>
    <div uk-slider="center:true;">
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-grid">
                @foreach($startups as $key=>$value)
                <li>
                    <div class="uk-child-width-1-1@s" uk-grid >
                        <div class="uk-card uk-card-default uk-card-large uk-card-body">
                            <table>
                                <tr>
                                    <td>
                                        <a href="#"><img  src="{{ asset('image/Taquion.png')}}" style="width:1250px;" /></a>
                                        {{$value->name}}
                                    </td>
                                    <td>
                                        <img src="{{ asset('image/foto.jpg') }}" style="width:30%; border-radius:100%" />
                                        <img src="{{ asset('image/foto.jpg') }}" style="width:30%; border-radius:100%" />
                                        <img src="{{ asset('image/foto.jpg') }}" style="width:30%; border-radius:100%" />
                                    </td>
                                 </tr>
                            </table>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" style="color:#00e6bf;" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" style="color:#00e6bf;" uk-slidenav-next uk-slider-item="next"></a>
        </div>
        
        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>
    <div uk-slider>
        <ul class="uk-slider-items"></ul>

        <div class="uk-child-width-1-1@s" uk-grid>
            <div class="uk-card uk-card-default uk-card-large uk-card-body">
                <h2 style='text-align:center;color:grey;font-family:arial;'>Mentores</h2> 
                <table class="uk-table uk-table-divider">
                    @foreach($mentores as $key=>$value)
                    <tr>
                        <td>
                            @if(false)
                            <img src="{{ asset('image/admin.jpg') }}" width="100px" >
                            @else
                            <img src="{{ asset('image/default.jpg') }}" width="100px" >
                            @endif
                        </td>
                        <td>
                            <label style="margin-left:10%;font-size:20px;">{{$value->name}}</label>
                        </td>
                        @auth
                        @if(\Auth::User()->nivel==0)
                            @if($value->ativo)
                        <td>
                            <a  class="btn btn-outline-success" href="{{route('ativar',$value->id)}}" id="btnAtivado">Ativo<span uk-icon="check"></span></a>
                        </td>
                            @else
                        <td>
                            <a  class="btn btn-outline-danger" href="{{route('ativar', $value->id)}}" id="btnDesativado">Desativo<span uk-icon="close"></span></a>
                        </td>
                            @endif 
                        @endif
                        @endauth
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection