@extends('layouts.app')
@section('content')
<script>
// Dados de pesquisa do blog
function enviarPesquisa(){
    document.forms['formSearch'].submit();
}
</script>
<div class="container">
    <div class="uk-child-width-1-1@s" uk-grid>
        <div class="uk-margin uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light" data-src="{{ asset('image/edital-logo.jpg')}}" uk-img>
            <a id='arrow-left-edital' href="/home" ><i class="fas fa-arrow-left fa-2x"></i></a>
            <div class="container">
                <h1 class="display-4">Blog </h1>
            </div>
        </div>
        <div>
        <h3 style="color:gray;">Últimas publicações</h3>
        @auth
        @if(\Auth::User()->nivel==0)
        <button class="uk-button uk-button-default" type="button" uk-icon="more-vertical"></button>
        <div uk-dropdown>
            <ul class="uk-nav uk-dropdown-nav">
                <li><a href="#offcanvas-usage" uk-toggle><button class="btn btn-outline-primary" >Modificar post</button></a></li>
                <li><a href="#modal-sections"  uk-toggle ><button class="btn btn-outline-primary" >Adicionar nova postagem</button></a></li>
            </ul>
        </div>
        {{-- BARRA LATERAL --}}
        <div id="offcanvas-usage" uk-offcanvas="flip: true; overlay: true" >
            <div class="uk-offcanvas-bar" style="width:20%;height:100%;" >
                <button class="uk-offcanvas-close" type="button" uk-close></button>
                <h3 style="text-align:center;font-family:segoe ui;">Escolha o post</h3>
                <div id='search-edital'>
                    <form action="{{url('/Showblog')}}" class="uk-search uk-search-default" style="background-color:white;width:200px;" method='post' name='formSearch'>
                        @csrf
                        <a onclick='enviarPesquisa();'  class="uk-search-toggle" style='color:black;' uk-search-icon></a>
                        <input class="uk-search-input"  type="search" placeholder="Pesquisar"name='search' id="search-edital" style="color:black;">
                    </form>
                </div>
                @foreach($Post as $key=>$value)
                <br><br>
                <div class="uk-button-group">
                    <div class="uk-inline">
                        <button class="btn" type="button" style="width:200px;" >{{$value->titulo}}</button>
                        <div uk-dropdown="mode: click;" style="width:'100%; background-color:white;" >
                            <ul class="uk-nav uk-dropdown-nav">
                                <!-- apagar e editar --> 
                                <a href="{{url('/edita',$value->id)}}"uk-icon="file-edit" style="color:#4bc7ff;text-align:center;" uk-tooltip="Editar postagem" >Editar</a>
                                <br><br>
                                <a href="javascript:function"  uk-icon="trash" onclick="confirmar({{$value->id}})" style="color:#4bc7ff;text-align:center;" uk-tooltip="Apagar postagem">Apagar</a> 
                                <!-- -------------- -->                            
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        @endauth
        </div>
        <!-- Modal de Adicionar postagem -->
        <div id="modal-sections" uk-modal>
            <div class="uk-modal-dialog">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                <div class="uk-modal-header">
                    <h2 class="uk-modal-title" style="text-align:center;color:gray;font-family:segoe ui;">Nova postagem</h2>
                </div>
                <div class="uk-modal-body">
                    <form action="{{route('criarBlog')}}" method="post" enctype="multipart/form-data">  
                        @csrf
                        <input class="uk-input uk-width-0-1" type="text" placeholder="Titulo" name="titulo"/>
                        <br><br>
                        <textarea class="uk-textarea uk-width-0-1" type="text" placeholder="Descrição" name="descricao" rows="2" style='resize: none;'></textarea>
                        <br><br>
                        <input class="uk-input uk-width-0-1" type="url" placeholder="Link" name="link"/>  
                        <br><br>
                        <div class="uk-margin">
                            <div uk-form-custom>
                                <input type="file" name="imagem">
                                <button class="uk-button uk-button-default" style="text-transform:capitalize;background-color:gray;color:#fff;"type="button" tabindex="-1">Selecionar mídia</button>
                            </div>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button class="uk-button uk-button-default uk-modal-close" type="button" >Cancelar</button>
                            <button class="uk-button uk-button-primary"  type="submit" style="background:#4bc7ff;">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        
        </div>
        <!-- fim modal de adicionar postagens -->
    <!-- alerta de erro no momento de inserir -->
    @if (session('success'))
        <div class="alert alert-success" >
            {{session('success')}}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" >
            {{session('error')}}
        </div>
    @endif
    <!-- ---------------------------------------- -->
    <!--- Parte off-canvas -->
    
    <!----------------------------------->
    <!-- slide -->
    <div uk-slider="center: true" style="background-color:white ">
        <div class="uk-position-relative uk-visible-toggle uk-light uk-margin-large-top" tabindex="-1">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-grid">
                @foreach($Post as $key=>$value)
                <li> 
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{ url('storage/Blog/'.$value->imagem) }}" style="width:800px;height:300px;" />
                        </div>
                    </div>      
                    <div class="uk-card-body" style="color:gray;">
                        <h2 class="uk-heading-divider" style="color:black;text-align:center;">{{$value->titulo}}</h2>
                        <p >{{$value->descricao}}</p>
                        <a class="uk-margin-remove" href="{{$value->link}}" target="_blank" style="color:#4bc7ff;">Link de Acesso</a>
                    </div>
                </li>
                @endforeach
            </ul>
        
            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-icon="icon: triangle-left; ratio: 4"s uk-slider-item="previous" style="opacity:0.65;-moz-opacity: 0.65;filter: alpha(opacity=65);"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-icon="icon: triangle-right; ratio: 4" uk-slider-item="next" style="opacity:0.65;-moz-opacity: 0.65;filter: alpha(opacity=65);"></a>
        </div>
        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-bottom"></ul>
    </div>
    {{-- FIM SLIDE --}}
    <h3 style="color:gray;">Publicações</h3>
    <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .uk-card;delay:350; repeat: true" uk-grid>
        @foreach($Post as $key=>$value)
        <div class="uk-card uk-card-hover uk-card-body">
            <div class="uk-card-media-left uk-cover-container">
                <img src="{{ url('storage/Blog/'.$value->imagem) }}"  />
                </div>
                <h3 class="uk-heading-divider" style="text-align:center;">{{$value->titulo}}</h3>
                <p>{{$value->descricao}}</p>
                <a class="uk-margin-remove" href="{{$value->link}}" target="_blank" style="color:#4bc7ff;">Link de Acesso</a>
           </div>
        @endforeach
        </div>
    </div>
</div> 
@endsection