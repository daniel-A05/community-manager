<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--<title>{{ config('app.name', 'Laravel') }}</title>-->
    <title>Comunnity Manager</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/uikit.js') }}" defer></script>
    <script src="{{ asset('js/uikit-icons.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>
    <script src="{{ asset('js/style.js')}}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/uikit.css') }}" rel="stylesheet">
    
    <style>
    .timeline{
  width: 250px;
  // overflow-y: auto; // Use it to allow scrolling.
  position: relative;
  top: 5;
  
  
  timeline-list{
    width: 100%;
    overflow: hidden;
    }
}

.timeline-list-item{
  transition: all 1s ease-in-out;
  list-style: none;
  cursor: pointer;
  //overflow: hidden;
  border-width: 0px 0px 0px .3rem;
  border-color: #333;
  border-style: solid;
  padding: .4rem .5rem;
  position: relative;
  // &::after{
  //   content: '';
  //   // display: block;
  //   position: absolute;
  //   left: 0;
  //   top: 0;
  //   width: 100%;
  //   height: 100%;
  //   background: red;
  //   z-index: -2;
  // }
  .timeline-list-item-date {
    display: block;
    position: relative;
    left: -1rem;
    &::before {
      transition: all 1s ease-in-out;
      content: '';
      display: inline-block;
      position: relative;
      left: -.35rem;
      top: .5rem;
      width: 1.4rem;
      height: 1.4rem;
      background: #333;
      // border: .3rem solid #555;
      border-radius: 50%;
    }
    
    &::after{
      transition: all 1s ease-in-out;
      content: '';
      display: inline-block;
      position: absolute;
      left: -.15rem;
      top: .7rem;
      background: #666;
      index: 5;
      height: 1rem;
      width: 1rem;
      border-radius: 50%;
    }
  }
  
  .timeline-list-item-link {
    // line-height: 1.5;
    transition: inherit;
    padding: 1rem .5rem;
    background: #999999;
  }
}

a.timeline-list-item-link{
  text-decoration: none;
  color: #333;
}

.timeline-list-item:hover{
  border-color: #4bc7ff;
  background: #4bc7ff;
  .timeline-list-item-date{
    &::after,&::before{
      border-radius: 10%;
    }
    
    &::before{
      background: #f54291;
    }
    &::after{
      background: #ffa0d2;
      
    }
  }
  .timeline-list-item-link{
    font-weight: bold;
  }
}
    </style>
</head>
<body style="font-family: 'Montserrat', sans-serif;">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        @guest
            @else
            <button class="uk-button uk-button-default" type="button" uk-toggle="target: #offcanvas-reveal">
                <span class="uk-margin-small-right" uk-icon="icon: thumbnails"></span>MENU
            </button>
            @endguest
            <div class="container">
                <!--<a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>-->
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <img style="width:40%" src="{{asset('image/logo-bioazul.png')}}" class="rounded"  >   
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div id="offcanvas-reveal" uk-offcanvas="mode: slide; overlay: false">
        <div class="uk-offcanvas-bar">
            <ul class="uk-nav uk-nav-default ">
                <button class="uk-offcanvas-close mb-4" type="button" uk-close></button>
                <li class="mb-4"><a  href="{{url('/home')}}" uk-tooltip="title: Home; pos: right"><span  uk-icon="icon:home"></span></a></li>
                <li class="mb-4"><a  href="{{route('Blog')}}" uk-tooltip="title: Blog; pos: right"><span  uk-icon="icon:hashtag"></span></a></li>
                <li class="mb-4"><a href="{{route('perfil')}}" uk-tooltip="title: Perfil; pos: right"><span  uk-icon="icon:user; check;"> </span></a></li>
                <li class="mb-4"><a href="{{url('Comunidade')}}" uk-tooltip="title: Membros da Comunidade; pos: right"><span  uk-icon="icon:users"></span></a></li>
                @auth
                        @if(\Auth::User()->nivel==0)
                <li class="mb-4"><a href="{{url('startup')}}" uk-tooltip="title: Usuários; pos: right"><span  uk-icon="icon:git-branch"></span></a></li>
                    @endif
                @endauth
                <li class="mb-4"> <a href="{{url('/ShowEdital')}}" uk-tooltip="title: Editais; pos: right"><span  uk-icon="icon:file-text"></span></a></li>
                <li class="mb-4"><a href="{{url('/chat')}}" uk-tooltip="title: Chat; pos: right"><span uk-icon="icon:comments"></span></a></li>
                <li class="mb-4"><a href="{{url('/Agenda')}}" uk-tooltip="title: Calendário; pos: right"><span uk-icon="icon:calendar"></span></a></li>
                <li class="uk-nav-divider"></li>
                <li><a href="#" uk-tooltip="title: Configurações; pos: right"><span  uk-icon="icon:cog"></span></a></li>
            </ul>
        </div>
    </div>
        <main class="py-4" style="background-color: #e3f2fd;">
            @yield('content')
        </main>
    </div>
</body>
</html>
