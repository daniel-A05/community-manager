@extends('layouts.app')

@section('content')
<html lang="pt-br">
<head>
    <title>Cadastrar Mentores</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"/>
    <!-- <script src="{{ asset('js/efeitos.js') }}" ></script> -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div style="position:absolute; margin: 3% 90% 0 0; background-color: #00e6bf; color: #fff; width: 1.8%; padding: 5px; border-radius: 100%;margin-left:10%;">
     <span style="font-size: 1.2em;"><a class="fas fa-chevron-left" style="color:white;" href="/home"></a></span>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div id="cabeçalho" style="margin-top: 2%;">
                        <h3 style="margin-bottom: -5%;color:#707070;text-align:center;">Cadastrar Mentores</h3>
                        <span style="font-size: 2em; margin-left:68%;color:#707070">
                            <i id="user" class="fas fa-user-plus "></i>
                        </span>
                    </div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="form-group row" style="margin-left: 7%; align-content: center; display: flex; justify-content: center;">
                                    <div class="col-sm-12" >
                                        <div class="col-auto">
                                              <div class="input-group mb-2">
                                                   <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                                    </div>
                                                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  id="name" name="name" placeholder="Nome completo"  value="{{ old('name') }}"  autofocus onkeypress="return ApenasLetras(event);">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="form-group row" style=" margin-left:7%;align-content: center; display: flex; justify-content: center;">
                                    <div class="col-sm-12">
                                        <div class="col-auto">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                                                     </div>
                                                         <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail" >
                                                </div>
                                         </div>
                                  </div>
                             </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="form-group row" style=" margin-left:7%;align-content: center; display: flex; justify-content: center;">
                                     <div class="col-sm-12">
                                        <div class="col-auto">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                                </div>
                                                <input id="password" type="text" maxlength="5" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  >
                                                <button  value="Gerar Senha" id="geradordesenha"  style="background-color:#00e6bf;border:white;font-family:arial;color:white;margin-left:2%; width:30%;border-radius:5%;" onclick="GerarSenha(); return: false" >Gerar Senha <i class="fas fa-key"></i></button>
                                                <input type="hidden" value="5" name="digitos"  class="campo2"/>                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="form-group row" style="margin-left:7%;align-content: center; display: flex; justify-content: center;">
                                    <div class="col-sm-12">
                                        <div class="col-auto">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                     <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                                </div>
                                                <input id="password-confirm" maxlength="5" type="text" class="form-control" name="password_confirmation"  placeholder="Confirme Sua Senha">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit"class="btn btn-primary btn-lg" style="background-color:#00e6bf;border:white;font-family:arial;">Enviar Dados <i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection