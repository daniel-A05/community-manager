@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- Palavra do gestor -->
        <div class="col-md-3">
            <div class="card p-1">
                <div class="card-header" style="">
                @if($existe == 1)
                    <img src="{{ url('storage/users/'.$foto[$id]->image) }}" style="float: left; width:90px;" class="rounded-circle" /> 
                @endif
                @if ($existe == 2)
                <script type="text/javascript">
                    window.location = "/Perfil";//here double curly bracket
                </script>
                @endif
                <dl class="uk-description-list">
                    
                        <h3>{{ Auth::user()->name }}</h3>
                        @if($existe == 1)
                        <dd>{{$foto[$id]->funcao}}</dd>
                        @endif
                        
                </dl>                
                </div>
                <div class="card-body">
                    <h3 class="text-center"> Palavra do gestor</h3>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">{{ session('status') }}</div>
                    @endif  
                    @auth
                        @if(\Auth::User()->nivel==0)
                        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                            <form action="{{url('/home/PalavraG')}}" method="get"> 
                                <h4 class="card-title text-center text-white">Criar Palavra do Gestor </h4>
                                <div class="card-header"><input type="text" class="form-control" name="titulo" placeholder="Título" ></div>
                                <div class="card-body"><textarea name="txt" class="form-control" placeholder="Mensagem"></textarea></div>
                                <div class="card-footer">
                                    <button type="submit" class="btn">{{ __('Enviar') }}</button>
                                </div>
                            </form>
                        </div>
                        @endif   
                    @endauth
                    @foreach($palavraGestor as $key => $value)
                    <div class="card  bg-default mb-3" style="max-width: 18rem;">
                        <div class="card-header">
                            {{$value->titulo}}
                            @auth
                                @if(\Auth::User()->nivel==0) 
                            <a uk-icon="icon:close" style="float:right;" href='javascript:func()' onclick="confirmar({{$value->id}})"></a>
                            @endif   
                    @endauth
                        </div>
                        <div class="card-body">
                            <p class="card-text">{{$value->txt}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Postagem -->
        <div class="col-md-6" style="float:center;" >
            <div class="card w-10 p-1">                
                <form method="post" action="{{route('CriarFeed')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="nome" value="{{ Auth::user()->name }}">
                    @if($existe == 1)
                    <input type="hidden" name="user_imagem" value="{{ $foto[$id]->image}}">
                    @endif
                    <!-- <input type="file" class="form-control" name="imagem" > -->
                    <textarea class="form-control" name="txt" placeholder="Texto..." required></textarea>
                    <div class="js-upload" uk-form-custom>
                            <input type="file" name="imagem" multiple>
                            <button class="uk-button uk-button-default" type="button" tabindex="-1"><span uk-icon="image"></span>
                            </button>
                        </div>
                    
                    
                    <div class="form-check form-check-inline" style="float:right">
                        <input class="btn btn-primary uk-margin-small-top"  type="submit" value="Publicar"/>
                    </div>
                </form>    
            </div>  
            @foreach ($Post as $key => $value)
            <div class="card w-10 p-1 uk-margin-small-top">
                <article class="uk-card uk-margin-top uk-margin-left uk-margin-right mb-3">
                    <header uk-grid>
                        <div class="uk-width-auto">
                            <img class="uk-comment-avatar" src="{{ url('storage/users/'.$value->user_image) }}" width="80" height="80" alt="">   
                        </div>
                        <div class="uk-width-expand">
                            @auth
                            @if(\Auth::User()->nivel==0 or \Auth::User()->id == $Post[$key]->user_id  )
                            <div class="uk-card-badge uk-icon"><a uk-icon="icon: trash" href='javascript:func()' onclick="confirmacao({{$value->id}})"></a></div>
                            <li></li>
                            @endif
                            @endauth
                            <h4 class="uk-card-title uk-margin-remove" style="color:#1983FD"><a class="uk-link-reset" href="#" style="color:#4bc7ff">{{$value->nome}}</a></h4>
                            <ul class="uk-card-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                <li><a href="#">{{$value->created_at}}</a></li>
                            </ul>
                            
                            <ul class="uk-iconnav uk-iconnav-vertical">
                                @if($existe == 1 && $value->imagem != null )
                                 <img src="{{ url('storage/feed/'.$value->imagem) }}" />
                                @endif
                            </ul>

                            <div class="uk-comment-body">
                                <p>{{$value->txt}}</p>
                            </div>
                        </div>
                    </header>
                    <hr class="uk-divider-icon">
                    <a href="#" class="btn"><span class="uk-margin-small-right" uk-icon="check"></span></a>
                </article>
            </div>        
            @endforeach

        </div>
        <!-- Eventos --> 
        <div class="col-md-3" style="float:left;">
            <div class="card w-10 p-1">
                <div class="card-header">
                    <h3 style="margin: 10px; font-family:font-family: 'Montserrat', sans-serif;">Eventos</h3>
                </div>
                <div class="card-body">
                    
                    
                    @foreach ($eventos as $key => $value)
                    <div class="timeline">
                        <ul class="timeline-list">
                            <a href="/Agenda" class="timeline-list-item-link"><li class="timeline-list-item">
                                <span class="timeline-list-item-date">{{$value->start_date}}</span>
                                {{$value->evento}} com {{$value->mentor}}
                            </li></a>
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    function confirmacao(id) {
        Swal.fire({
        title: 'Confirmação:',
        text: "Deseja realmente apagar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, Apagar post!'
        }).then((result) => {
            if (result.value) {
                $id = id;
                Swal.fire(
                'Apagado!',
                'Este post foi apagado.',
                'success'
                )
                window.location.href="/home/apagar/"+id;
            }    
        })
    }

    function confirmar(id) {
        Swal.fire({
        title: 'Confirmação:',
        text: "Deseja realmente apagar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, Apagar aviso!'
        }).then((result) => {
            if (result.value) {
                $id = id;
                Swal.fire(
                'Apagado!',
                'Este aviso foi apagado.',
                'success'
                )
                window.location.href="/home/apaga/"+id;
            }    
        })
    }
</script>

@endsection