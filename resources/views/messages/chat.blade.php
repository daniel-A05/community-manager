<div class="message-wrapper">
        <ul class="messages">
            @foreach($messages as $message)
                <li class="message clearfix">
                    {{--if message from id is equal to auth id then it is sent by logged in user --}}
                    <div class="{{ ($message->from == Auth::id()) ? 'sent' : 'received' }}">
                        <div class="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p>{{ $message->message}}</p>
                            <p class="date">{{
                                date('d M y, H:i a', strtotime($message->created_at)-10800) }}</p>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item"href="javascript:function" onclick="confirmar({{$message->id}})" >Excluir</a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="input-text">
        <input type="text" name="message" class="submit"/>
    </div>
    