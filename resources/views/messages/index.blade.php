@extends('layouts.app')
@section('content')
<script>
    // Dados de pesquisa do chat
    function enviarPesquisa(){
        document.forms['formSearch'].submit();
    }
</script>
    <div class="container-fluid" style="background-color: white">
        <div class="row">
            <div class="col-md-4">
                <div class="user-wrapper">
                    <ul class="users">
                        <div id="search-user">
                            <form action="{{url('/ShowChat')}}" class="uk-search uk-search-default" style="margin-left:auto;width:500px;" method='GET' name='formSearch'>
                                <a onclick='enviarPesquisa();' uk-search-icon></a>
                                <input class="uk-search-input" type="search" placeholder="Pesquisar" name='search' id="search-chat">
                            </form>
                        </div>
                        <img src="{{ asset('image/user.jpg') }}" alt="" class="media-object" style="width: 64px;border-radius: 64px;">
                        {{ Auth::user()->name }}
                        <p style="margin-left:20%;margin-top:-4%;">Disponível</p>
                        <h4 style="font-weight:bolder;text-align:center;">Contatos</h4>
                        
                        <!-- --------------------------------------------------------------------------------- -->
                        <button class="uk-button uk-button-default" type="button" uk-toggle="target: #offcanvas-flip" style="color:white;background:#4bc7ff;margin-left:70%;margin-top:-60%;">Online</button>
                        @foreach($users as $user)
                        <!-- off-canvas do onlines -->
                        <div id="offcanvas-flip" uk-offcanvas="flip: true; overlay: true">
                            <div class="uk-offcanvas-bar">
                                <button class="uk-offcanvas-close" type="button" uk-close></button>
                                <div id="search-user">
                                    <form action="{{url('/ShowChat')}}" class="uk-search uk-search-default" style="margin-left:auto;width:500px;" method='GET' name='formSearch'>
                                        <a onclick='enviarPesquisa();' uk-search-icon></a>
                                        <input class="uk-search-input" type="search" placeholder="Pesquisar" name='search' id="search-chat">
                                    </form>
                                </div>
                                <br><br>
                                <li class="user" id="{{ $user->id }}">
                                    {{--will show unread count notification--}}
                                    @if($user->unread)
                                        <span class="pending">{{ $user->unread }}</span>
                                    @endif
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{ asset('image/user.jpg') }}" alt="" class="media-object">
                                        </div>
                                        <div class="media-body">
                                            <h5 style="font-weight:bolder;" class="name">{{ $user->name }}</h5>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!------------------------------- -->
                        <li class="user" id="{{ $user->id }}">
                            {{--will show unread count notification--}}
                            @if($user->unread)
                                <span class="pending">{{ $user->unread }}</span>
                            @endif
                            <div class="media">
                                <div class="media-left">
                                    <img src="{{ asset('image/user.jpg') }}" alt="" class="media-object">
                                </div>
                                <div class="media-body">
                                    <h5 style="font-weight:bolder;" class="name">{{ $user->name }}</h5>
                                    <p class="email">{{ $user->email }}</p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- Parte Inicial da tela quando não tem mensagens -->
            <div class="col-md-8" id="messages" >
            <img src="{{ asset('image/walp.png')}}" style="margin-top:-18%;margin-left:10%;"/>
            </div>
        </div>
    </div>
@endsection
