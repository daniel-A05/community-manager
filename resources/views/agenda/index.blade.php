<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Scripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="https://kit.fontawesome.com/3795336791.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Uikit -->
  <link rel="stylesheet" href="{{ asset('css/uikit.css') }}">
  <script src="{{ asset('js/uikit.js') }}"></script>
  <script src="{{ asset('js/uikit-icons.js') }}"></script>

  <!-- FullCalendar -->
  <link href="{{ asset('css/fullCalendar/core-main.css') }}" rel='stylesheet' />
  <link href="{{ asset('css/fullCalendar/day-main.css') }}" rel='stylesheet' />
  <script src="{{ asset('js/fullCalendar/moment.min.js') }}"></script>
  <script src="{{ asset('js/fullCalendar/core-main.js') }}"></script>
  <script src="{{ asset('js/fullCalendar/inte-main.js') }}"></script>
  <script src="{{ asset('js/fullCalendar/day-main.js') }}"></script>
  <script src="{{ asset('js/fullCalendar/pt-br.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('css/fullCalendar/datepicker.min.css')}}">

  {{-- CSS externo --}}
  <link rel="stylesheet" href="{{asset('css/main.css')}}">
</head>
<body>
<!-- NAVBAR E MENU -->
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    @guest
        @else
        <button class="uk-button uk-button-default" type="button" uk-toggle="target: #offcanvas-reveal">
            <span class="uk-margin-small-right" uk-icon="icon: thumbnails"></span>MENU
        </button>
        @endguest
        <div class="container">
            <!--<a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>-->
            <a class="navbar-brand" href="{{ url('/home') }}">
                <img style="width:40%" src="{{asset('image/logo-bioazul.png')}}" class="rounded"  >   
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <div id="offcanvas-reveal" uk-offcanvas="mode: slide; overlay: false">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-default ">
            <button class="uk-offcanvas-close mb-4" type="button" uk-close></button>
            <li class="mb-4"><a  href="{{url('/home')}}" uk-tooltip="title: Home; pos: right"><span  uk-icon="icon:home"></span></a></li>
            <li class="mb-4"><a  href="{{route('Blog')}}" uk-tooltip="title: Blog; pos: right"><span  uk-icon="icon:comments"></span></a></li>
            <li class="mb-4"><a href="{{route('perfil')}}" uk-tooltip="title: Perfil; pos: right"><span  uk-icon="icon:user; check;"> </span></a></li>
            <li class="mb-4"><a href="{{url('Comunidade')}}" uk-tooltip="title: Membros da Comunidade; pos: right"><span  uk-icon="icon:users"></span></a></li>
            <li class="mb-4"><a href="{{url('startup')}}" uk-tooltip="title: Usuários; pos: right"><span  uk-icon="icon:git-branch"></span></a></li>
            <li class="mb-4"> <a href="{{url('/ShowEdital')}}" uk-tooltip="title: Editais; pos: right"><span  uk-icon="icon:file-text"></span></a></li>
            <li class="mb-4"><a href="#" uk-tooltip="title: Eventos; pos: right"><span uk-icon="icon:bookmark"></span></a></li>
            <li class="mb-4"><a href="{{url('/Agenda')}}" uk-tooltip="title: Calendário; pos: right"><span uk-icon="icon:calendar"></span></a></li>
            <li class="uk-nav-divider"></li>
            <li><a href="#" uk-tooltip="title: Configurações; pos: right"><span  uk-icon="icon:cog"></span></a></li>
        </ul>
    </div>
  </div>
{{-- FIM NAVBAR E MENU --}}
  @foreach($agenda as $gen)
    <button id='desc{{$gen->id}}' style='display: none;' class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #offcanvas-flip{{$gen->id}}"></button>

    <div id="offcanvas-flip{{$gen->id}}" uk-offcanvas="flip: flip;">
      <div style='width: 300px;' class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <h4 class="uk-comment-title uk-margin-remove">{{$gen->titulo}}</h4>
        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-top">
          <li>{{$gen->start_date}}</li>
          <li>{{$gen->hora_criacao}}</li>
        </ul>
        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-top">
          <li>{{$gen->evento}}</li>
        </ul>
        <div class="uk-comment-body">
          <p>{{$gen->descricao}}</p>
        </div>
        <ul uk-accordion>
          <li>
            <a class="uk-accordion-title" href="#">Atualizar</a>
            <div class="uk-accordion-content">
                        <form action="{{ route('AtualizarEvento', $gen->id) }}" method='post' name='formEditarEvento'>
                          @csrf
                          <div class="uk-margin" action='' method='post'>
                              <div class="uk-inline">
                                  <a class="uk-form-icon" href="#"><i class="far fa-edit"></i></a>
                                  <input class="uk-input" type="text" name='titulo' placeholder='Adicionar título...' value='{{$gen->titulo}}'>
                              </div>
                          </div>

                          <div class="uk-margin">
                            <div uk-form-custom="target: > * > span:first-child">
                                <select name='evento' required>
                                  <option value=''>Add. o evento...</option>
                                  <option value='mentoria'>Mentoria</option>
                                  <option value='eventos'>Eventos</option>
                                  <option value='cursos'>Cursos</option>
                                  <option value='oficinas'>Oficinas</option>
                                  <option value='reunioes'>Reuniões</option>
                                </select>
                                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                                    <span></span>
                                </button>
                            </div>
                          </div>

                          <div class="uk-margin">
                            <div uk-form-custom="target: > * > span:first-child">
                                  <select name='mentor' required>
                                    <option value=''>Add. mentor...</option>
                                    <option value='Luiz Eduardo'>Luiz Eduardo</option>
                                    <option value='Maruska'>Maruska</option>
                                    <option value='Marina'>Marina Lecas</option>
                                    <option value='Diego'>Diego Saulo</option>
                                  </select>
                                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                                    <span></span>
                                </button>
                            </div>
                          </div>

                          <div class="uk-margin">
                            <div uk-form-custom="target: > * > span:first-child">
                                  <select name='local' required>
                                    <option value=''>Add. local...</option>
                                    <option value='sala 1'>Sala 1</option>
                                    <option value='sala 2'>Sala 2</option>
                                    <option value='sala 3'>Sala 3</option>
                                    <option value='launcher'>Launcher</option>
                                  </select>
                                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                                    <span></span>
                                </button>
                            </div>
                          </div>

                          <div class="uk-margin" action='' method='post'>
                              <div class="uk-inline">
                                  <a class="uk-form-icon" href="#"><i class="far fa-edit"></i></a>
                                  <input class="uk-input" type="text" name='descricao' placeholder='Adicionar descrição...' value='{{$gen->descricao}}'>
                              </div>
                          </div>

                          <div class="uk-margin">
                            <div uk-form-custom="target: > * > span:first-child">
                                  <select name='equipamento' required>
                                    <option value=''>Add. equipamento</option>
                                    <option value='monitor'>Monitor</option>
                                    <option value='notebook 1'>Notebook 1</option>
                                    <option value='notebook 2'>Notebook 2</option>
                                    <option value='microfone'>Microfone</option>
                                  </select>
                                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                                    <span></span>
                                </button>
                            </div>
                          </div>

                          <div class="uk-margin">
                            <label>Início</label><br>
                              <input type="date" name="start_date" id="start_date" value='{{$gen->start_date}}' required><br>
                            <label>Fim</label><br>
                              <input type="date" name="end_date" id="end_date" value='{{$gen->end_date}}' required>
                          </div>
              
                          <div class="uk-margin">
                            <input type='hidden' name='color' value='{{$gen->color}}'>
                          </div>
                          
                          <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                              @if($gen->privado)
                                <label><input class="uk-checkbox" type="checkbox" name='privado' value='1' checked>Privado</label>
                              @else
                                <label><input class="uk-checkbox" type="checkbox" name='privado' value='1'>Privado</label>
                              @endif
                          </div>

                          <button class="uk-button uk-button-default">Salvar</button>
                        </form>

            </div>
          </li>
          <li>
            <a class="uk-accordion-title" href="#">Remover</a>
            <div class="uk-accordion-content">
              <div class="uk-comment-body">
                <p>Deseja realmente remover ?</p>
                </div>
                  <p uk-margin>
                    <a class="uk-button uk-button-danger" onclick='removerEvento({{$gen->id}});' href='javascript:function'>Remover</a>
                  </p>
                </div>
          </li>
        </ul>
      </div>
    </div>
  @endforeach

  <div class="uk-text-center" uk-grid="masonry: true">
      <div class='uk-width-1-6@m uk-margin-left uk-margin-large-top' style="height: 400px;">
          <ul class="uk-nav uk-nav-default uk-width-expand@m">
              <div class='uk-width-expand@m'>
                {{-- BARRA LATERAL --}}
                <form id='formSearA' action="{{url('/Agenda')}}" name='formAgendasearch' method='post'>
                    @csrf
                    <div class="uk-margin">
                        <div class="uk-inline">
                            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: search" onclick='pesquisaAgenda();' ></a>
                            <input class="uk-input" type="text" name='searchAgenda' id='searchAgenda' placeholder='Pesquise' >
                        </div>
                    </div>
                </form>
                <div id='eventosList' class='uk-panel uk-panel-scrollable' style='position: relative; resize: none; height: 400px; width: 210px;'>
                    @foreach($agenda as $gen => $value)
                      <div class="uk-margin">    
                        <div id='listaAgenda' class="uk-card agenda-eventos uk-card-default uk-card-body uk-card-small">
                          <span class='agenda-cor-evento uk-position-top-left' style='background-color: {{$value->color}};'></span>
                          <span id='#desc{{$value->id}}' uk-toggle="target: #offcanvas-flip{{$value->id}}" class='agenda-mais-evento uk-position-top-right'><i class="fas fa-ellipsis-h"></i></span>
                            {{$value->titulo}}
                        </div>
                      </div>
                    @endforeach
                </div>
            </div>
          </ul>
      </div>

      <div class="uk-width-expand@m uk-margin-large-right">
        <div id='calendar' style="background: white" class="uk-height-1-1 uk-margin-top uk-margin-medium-bottom"></div> 
      </div>
  </div>
  
</body>
<script>
    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'pt-br',
        plugins: ['interaction', 'dayGrid'],
        selectable: true,
        header: { 
          right: 'prev,next today',
          left: 'title',
        },
        height: 590,
        eventLimit: true,
        eventClick: function(info) {
          var eventObj = info.event;
          $("#desc"+eventObj.id).click();
        },
        events: [
          <?php foreach($agenda as $gen){?>
            {
              id: "{{ $gen["id"] }}",
              title: "{{ $gen["titulo"] }}",
              start: "{{ $gen["start_date"] }}",
              end: "{{ $gen["end_date"] }}",
              backgroundColor: "{{ $gen["color"] }}",
              borderColor: 'transparent',
              textColor: '#fff'
            },
          <?php }?>
        ],
          select: function(event) {
            var data = new Date();
            var hora = data.getHours();   // 0-23
            var min  = data.getMinutes(); // 0-59
            var hora_criacao = hora + ':' + min;

            var today = moment(data).format('YYYY/MM/DD');

            var start_date = event.start;
            var diaClicado = event.start.getDate();
            start_date = moment(start_date).format('YYYY/MM/DD');
            var end_date = event.end;
            end_date = moment(end_date).format('YYYY/MM/DD');
            if(start_date < today){

              return false;
            }else{
              (async () => {

                const { value: agendar } = await Swal.fire({
                  html:
                    "<form id='agenda' method='get' action='/AdicionarEvento'>"+
                      "<div class='uk-margin' action='' method='post'>"+
                        "<div class='uk-inline'>"+
                          "<a class='uk-form-icon' href='#'><i class='far fa-edit'></i></a>"+
                          "<input class='uk-input' type='text' name='titulo' placeholder='Adicionar título...'>"+
                        "</div>"+
                      "</div>"+
                      "<div class='uk-margin'>"+
                        "<div uk-form-custom='target: > * > span:first-child'>"+
                            "<select name='evento' required>"+
                              "<option value=''>Add. o evento...</option>"+
                              "<option value='mentoria'>Mentoria</option>"+
                              "<option value='eventos'>Eventos</option>"+
                              "<option value='cursos'>Cursos</option>"+
                              "<option value='oficinas'>Oficinas</option>"+
                              "<option value='reunioes'>Reuniões</option>"+
                            "</select>"+
                            "<button class='uk-button uk-button-default' type='button' tabindex='-1'>"+
                                "<span></span>"+
                            "</button>"+
                        "</div>"+
                      "</div>"+
                      "<div class='uk-margin'>"+
                        "<div uk-form-custom='target: > * > span:first-child'>"+
                              "<select name='mentor' required>"+
                                "<option value=''>Add. mentor...</option>"+
                                "<option value='Luiz Eduardo'>Luiz Eduardo</option>"+
                                "<option value='Maruska'>Maruska</option>"+
                                "<option value='Marina'>Marina Lecas</option>"+
                                "<option value='Diego'>Diego Saulo</option>"+
                              "</select>"+
                            "<button class='uk-button uk-button-default' type='button' tabindex='-1'>"+
                                "<span></span>"+
                            "</button>"+
                        "</div>"+
                      "</div>"+
                      "<div class='uk-margin'>"+
                        "<div uk-form-custom='target: > * > span:first-child'>"+
                              "<select name='local' required>"+
                                "<option value=''>Add. local...</option>"+
                                "<option value='sala 1'>Sala 1</option>"+
                                "<option value='sala 2'>Sala 2</option>"+
                                "<option value='sala 3'>Sala 3</option>"+
                                "<option value='launcher'>Launcher</option>"+
                              "</select>"+
                            "<button class='uk-button uk-button-default' type='button' tabindex='-1'>"+
                                "<span></span>"+
                            "</button>"+
                        "</div>"+
                      "</div>"+
                      "<div class='uk-margin' action='' method='post'>"+
                          "<div class='uk-inline'>"+
                              "<a class='uk-form-icon' href='#'><i class='far fa-edit'></i></a>"+
                              "<input class='uk-input' type='text' name='descricao' placeholder='Adicionar descrição...'>"+
                          "</div>"+
                      "</div>"+
                      "<div class='uk-margin'>"+
                        "<div uk-form-custom='target: > * > span:first-child'>"+
                              "<select name='equipamento' required>"+
                                "<option value=''>Add. equipamento</option>"+
                                "<option value='monitor'>Monitor</option>"+
                                "<option value='notebook 1'>Notebook 1</option>"+
                                "<option value='notebook 2'>Notebook 2</option>"+
                                "<option value='microfone'>Microfone</option>"+
                              "</select>"+
                            "<button class='uk-button uk-button-default' type='button' tabindex='-1'>"+
                                "<span></span>"+
                            "</button>"+
                        "</div>"+
                      "</div>"+
                      "<div class='uk-margin'>"+
                        "<input type='hidden' name='start_date' value='"+start_date+"' >"+
                        "<input type='hidden' name='end_date' value='"+end_date+"' >"+
                        "<input type='hidden' name='color'>"+
                        "<input type='hidden' name='hora_criacao' value='"+hora_criacao+"' >"+
                      "</div>"+
                      "<div class='uk-margin uk-grid-small uk-child-width-auto uk-grid'>"+        
                        "<label><input class='uk-checkbox' type='checkbox' name='privado' value='1'>Privado</label>"+
                      "</div>"+
                      "<button class='uk-button uk-button-default'>Salvar</button>"+
                    "</form>",
                    showConfirmButton: false
                })

              })()
            }
          }
      });
      calendar.render();
    });  

  function removerEvento(id){
    window.location.href = "/RemoverEvento/"+id;
  }

  function pesquisaAgenda(){
      document.forms['formAgendasearch'].submit();
  }

  document.getElementById('start_date').min = new Date().toISOString().split("T")[0];
  document.getElementById('end_date').min = new Date().toISOString().split("T")[0];

</script>
</html>
