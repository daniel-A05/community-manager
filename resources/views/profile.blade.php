@extends("layouts.app")

@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 ">
            <div class="card">
                <div class="card-header bg-primary text-white">Formulário de inserção dos dados pessoais</div>
                @if (session('success'))
                    <div class="alert alert-success" >
                        {{session('success')}}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" >
                        {{session('error')}}
                    </div>
                @endif
                <div class="card-body">
                    <form method="POST" action="/Perfil/dadosPessoais" enctype="multipart/form-data">
                        @csrf
                        <!--Nome Completo --> 
                        <div class="form-group row">
                            <label for="nomeC" class="col-sm-4 col-form-label text-md-right">Nome Completo:</label>
                            <div class="col-md-6">
                                <input id="name" class="form-control" type="text"  name="nomeC" value="{{old('nomeC')}}"  autofocus >
                            </div>
                        </div>
                        <!--Data Nascimento --> 
                        <div class="form-group row">
                            <label for="dataNasc" class="col-sm-4 col-form-label text-md-right">Data de Nascimento:</label>
                            <div class="col-md-6">
                                <input id="dataNasc" name="dataNasc" class="form-control" type="date" value="{{old('date')}}"  autofocus >
                            </div>
                        </div>
                        <!--Função --> 
                        <div class="form-group row">
                            <label for="funcao" class="col-sm-4 col-form-label text-md-right">Função:</label>
                            <div class="col-md-6">
                                <input id="funcao"  name="funcao"class="form-control" type="text" value="{{old('funcao')}}"  autofocus >
                            </div>
                        </div>
                        <!--Sobre --> 
                        <div class="form-group row">
                            <label for="sobre" class="col-sm-4 col-form-label text-md-right">Sobre:</label>
                            <div class="col-md-6">
                                <input id="sobre" name="sobre" class="form-control" type="text" value="{{old('sobre')}}"  autofocus >
                            </div>
                        </div>
                        <!--Foto Perfil --> 
                        <div class="form-group row">
                            <label for="image" class="col-sm-4 col-form-label text-md-right">Foto perfil:</label>
                            <div class="col-md-6">
                                <input id="image" type="file"  name="image" value="{{old('image')}}"  autofocus >
                            </div>
                        </div>
                        <!--input que receberá id do usuário  --> 
                        <input type="hidden" name="user_id" value="">
                        <!--Envio  -->
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button class="btn btn-primary" type="submit" >Inserir dados</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 