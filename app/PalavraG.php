<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PalavraG extends Model
{
    protected $fillable = [
        'id','titulo', 'txt',
    ];

    public function getId(){return $this->id;}
    public function getTitulo(){return $this->titulo;}
    public function getTxt(){return $this->txt;}
    
}
