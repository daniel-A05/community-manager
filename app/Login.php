<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $fillable=['id','user_id','nomeC','dataNasc','funcao','image','sobre'];
    public function getId(){return $this->id;}
    public function getUserId(){return $this->belongsTo('App\User');}
    public function getNomeC(){return $this->nomeC;}
    public function getFuncao(){return $this->funcao;}
    public function getData_nascimento(){return $this->dataNasc;}
    public function getImage(){return $this->image;}
    public function getSobre(){return $this->sobre;}
}
