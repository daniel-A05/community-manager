<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Edital;

class EditalController extends Controller
{
    public function index(){

        $edital = Edital::all();
        //dd($edital);
        return view('edital.index', compact('edital'));
    }

    public function inserir(Request $request){
        $request->validate([
            'titulo' => 'required|string',
            'descricao' => 'required|string',
            'link' => 'required|string'
        ]);
        
        $data = $request->all();
        

        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
            $name= date("h_i_s");
            $ext = $request->imagem->extension(); //pegando extenção do arquivo
            $nameFile = "{$name}.{$ext}"; 

            $upload = $request->imagem->storeAs('public/edital',$nameFile);
            $data['imagem'] = $nameFile;
           
            if (!$upload) {
                return redirect()->back()->with('error','Falha ao fazer o upload da imagem');
            }      
        } 
        //dd($data);
        Edital::create($data);
        return back();
    }

    public function detailEdital($id){
        $edital = Edital::find($id);
        return view('edital.detail', compact('edital'));
    }
    
    public function update(Request $request, $id){
        $request->validate([
            'titulo' => 'required|string',
            'descricao' => 'required|string',
            'link' => 'required|string'
        ]);
        
        $edital = Edital::find($id);
        $arquivo = $edital->imagem;
        Storage::delete('edital/'.$arquivo);
        
        $edital->titulo = $request->get('titulo');
        $edital->descricao = $request->get('descricao');
        $edital->link = $request->get('link');
        $edital->save();
        return back();
    }

    public function remove($id){
        $edital = Edital::find($id);
        $arquivo = $edital->imagem;
        Storage::delete('edital/'.$arquivo);
        $edital->delete();
        return redirect()->route('show');
    }

}
