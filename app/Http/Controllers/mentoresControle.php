<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class mentoresControle extends Controller
{
  public function index(){
    $Post = \App\User::all();
    $startups = $Post->where("nivel", 1);
    $mentores = $Post->where("nivel", 2);

    //dd($mentores);
    return view('membrosComunidade', compact('startups','mentores'));
  }
  public function destroy($id){
      $produto = User::find($id); //find->where (fazendo referência)
      $produto->delete(); //deletando dados
      return view("Mentores"); //valores
  }
  public function edit($id){ //editando os dados
      $editar=User::find($id);
      //dd($editar);
      return view('editarMentor', compact('editar')); //compactando com os dados do foreach

  }
  public function update(Request $request, $id){
    $request->validate([
        'name' => 'required',
        'email' => 'required',
    ]);
    $modelo=User::find($id); 
    $modelo->name=$request->get('name');
    $modelo->email=$request->get('email');
    $modelo->save();
    return "editou";
  }
  //para ativar e desativar mentores
  public function show($id){
    $mentores = User::find($id);
    if($mentores->ativo==0){
      $mentores->ativo=1;
    }else{
      $mentores->ativo=0;
    }
    $mentores->save();
    return redirect()->back();
  }

}
