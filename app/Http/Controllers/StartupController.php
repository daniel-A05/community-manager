<?php

namespace App\Http\Controllers;

use App\User;
use App\Login;
use Illuminate\Support\Facades\Hash;
use App\Rules\Uppercase;
use Illuminate\Http\Request;

class StartupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $startups = User::latest()->paginate(5);
        return view('startup.index', compact('startups'))
                ->with('i', (request()->input('page', 1)-1)*5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('startup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max: 20',
            'email' => 'required|email',
            'password' => 'required'//Depois colocar o new Uppercase aqui
        ]);
        //dd($request->all());
        
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'nivel' => $request->nivel,
        ]);
            return redirect()->route('startup.index')
                    ->with('success', 'Nova startup adicionada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $startup = Startup::find($id);
        if($startup->ativo == 0)
        {
            $startup->ativo = 1;
        }
        else
        {
            $startup->ativo = 0;
        }
        $startup->save();
        return redirect()->back();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
