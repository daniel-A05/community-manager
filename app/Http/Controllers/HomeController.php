<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Feed;
use \App\Agenda;
use \App\PalavraG;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $eventos = \App\Agenda::all();
        $palavraGestor = \App\PalavraG::all();
        $foto = \App\Login::all(); //DADOS PESSOAIS
        $Post = \App\Feed::all()->sortByDesc("id"); //POSTAGENS CRIADAS
        $user = auth()->user();
        $id = 0;
        $existe = 2;
        //VERIFICAÇÃO SE O USUÁRIO JÁ TEM CADASTRO 
        for ($i=0; $i < $foto->count(); $i++) { 
            if ($foto[$i]->user_id == $user->id) {
                $existe = 1; //Existe foto no banco
                $id = $i;//ARMAZENANDO ID DO ARRAY
            }
        }
        
        if ($existe == 1){
            return view('home', compact('Post','palavraGestor','foto','existe','id','eventos'));
        }else {
            return view('home', compact('Post','palavraGestor','foto','existe'));
        }
    }
}
