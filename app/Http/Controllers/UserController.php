<?php

namespace App\Http\Controllers;

use App\User;
use App\Login;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function updateFoto(Request $request){
        $user = auth()->user();
        $data = $request->all();
        $data['user_id'] = $user->id;
        $total = \App\Login::all();
        //$login = \App\Login::find(0);
        $existe = 2;
        if ($total->count() != 0) {
            for ($i=0; $i < $total->count(); $i++) { 
                if ($total[$i]->user_id == $user->id) {
                    $existe = 1; //Existe dados no banco
                }
            }
        }
        
        if($existe == 1){
            return redirect()
                    ->back()
                    ->with('error', 'Você já preencheu seus dados...');
        }else {
            //FOTOS 
            $data['image'] = $user->image;
           
            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                if ($user->image) {
                    $nameFile = $user->image;
                } else {
                    $name= $user->email;
                    $ext = $request->image->extension(); //pegando extenção do arquivo
                    $nameFile = "{$name}.{$ext}"; 
                }

                $upload = $request->image->storeAs('public/users', $nameFile);
                $data['image'] = $nameFile;

                if (!$upload) {
                    return redirect()->back()->with('error','Falha ao fazer o upload da imagem');
                }      
            } 
            \App\Login::create($data);
            return redirect()
                        ->route('perfil')
                        ->with('success', 'Cadastro inserido com sucesso!');
        }
    }

    public function perfil(){
        $user = auth()->user();
        $total = \App\Login::all();
        //$login = \App\Login::find(0);
        $existe = 2;
        if ($total->count() != 0) {
            for ($i=0; $i < $total->count(); $i++) { 
                if ($total[$i]->user_id == $user->id) {
                    $existe = 1; //Existe dados no banco
                    $id = $i;
                }
            }
        } 
        
        if ($existe == 1) {
            
            //dd(\App\Login::find($id+1)->user_id);
            return view('perfil', compact('total','id'));
        } else {
            return view('profile');
        }
    }

}
