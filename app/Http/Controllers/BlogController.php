<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Input; //chamando input

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Post = Blog::all();
    //  dd($Post);
        return view('Blog.blog', compact('Post'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        //Inserindo imagem com laravel
        //dd($request->imagem);
        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
            $name= date("h_i_s");
            $ext = $request->imagem->extension(); //pegando extenção do arquivo
            $nameFile = "{$name}.{$ext}"; 

            $upload = $request->imagem->storeAs('public/Blog',$nameFile);
            $data['imagem'] = $nameFile;
           
            if (!$upload) {
                return redirect()->back()->with('error','Falha ao fazer o upload da imagem');
            }      
        } 
        //dd($data);
        $update = Blog::create($data);
        return redirect()
                    ->route('Blog')
                    ->with('success', 'Postagem inserida com sucesso!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $q = Input::get('search');
        if($q != ''){
            $Post = Blog::where('titulo', 'LIKE' ,'%'.$q.'%')->get();
            if(count($Post) >= 1 ){
                return view('Blog.blog', compact('Post'));
            }
        }
        return view('Blog.blog');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editar=Blog::find($id);
        return view('editarBlog', compact('editar')); //compactando com os dados do foreach

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['titulo' => 'required','descricao' => 'required','link' => 'required',]);
        $modelo=Blog::find($id); 
        $modelo->titulo=$request->get('titulo');
        $modelo->descricao=$request->get('descricao');
        $modelo->link=$request->get('link');
        $modelo->save();
        return redirect()
                ->route('Blog')
                ->with('success', 'Sua postagem foi editada com sucesso!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Blog::find($id); //find->where (fazendo referência)
      $arquivo=$produto->imagem;
      Storage::delete('Blog/'.$arquivo);
      $produto->delete();

      return back();

    }

    
}
