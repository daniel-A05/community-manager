<?php

namespace App\Http\Controllers;

use \App\Feed;
use Illuminate\Http\Request;

class FeedControle extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->all();
        
        $data = [
            "_token" => $request->_token,
            "user_id"=> auth()->user()->id,
            "user_image"=> $request->user_imagem,
            "nome" => auth()->user()->name,
            "txt" => $request->txt,
            "imagem" => $request->imagem
        ];
        
        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
            $name= date("h_i_s");
            $ext = $request->imagem->extension(); //pegando extenção do arquivo
            $nameFile = "{$name}.{$ext}"; 

            $upload = $request->imagem->storeAs('public/feed',$nameFile);
            $data['imagem'] = $nameFile;
           
            if (!$upload) {
                return redirect()->back()->with('error','Falha ao fazer o upload da imagem');
            }      
        } 
        
        //dd($data);
        $update = \App\Feed::create($data);

        return redirect()
                    ->route('home')
                    ->with('success', 'Postagem inserida com sucesso!');
        

        //dd($request->hasFile('imagem'));
        
        return back();
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apagar = \App\Feed::find($id);
       // dd($apagar);
        $apagar->delete();
        return back();
    }
}
