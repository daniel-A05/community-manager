<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'id','nome', 'txt','imagem','user_id','user_image'
    ];

    public function getId(){return $this->id;}
    public function getUserId(){return $this->user_id;}
    public function getUserImage(){return $this->user_image;}
    public function getNome(){return $this->nome;}
    public function getTxt(){return $this->txt;}
    public function getImagem(){return $this->imagem;}
    
}
