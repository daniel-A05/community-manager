<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentores extends Model{
    protected $fillable=['id','nome','funcao','data_nascimento','email','senha'];
    public function getId(){return $this->id;}
    public function getNome(){return $this->nome;}
    public function getFuncao(){return $this->funcao;}
    public function getData_nascimento(){return $this->data_nascimento;}
    public function getEmail(){return $this->email;}
    public function getSenha(){return $this->senha;}
}
