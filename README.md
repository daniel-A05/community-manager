<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

Sistema direcionado a uma Gestão de comunidade, ainda em desenvolvimento. Projeto constituidos outros integrantes de equipe. Feito com o Laravel 5.7

INSTRUÇÕES PARA O FUNCIONAMENTO DO PROJETO:

DENTRO DA PASTA DO PROJETO:
composer install
cp .env.example .env 
php artisan key:generate

php artisan migrate:refresh
php artisan db:seed