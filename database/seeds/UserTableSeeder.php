<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ninho de Inovação',
            'email' => 'iccninho@gmail.com',
            'password' => Hash::make('ninho2019'),
            'nivel' => '0',
        ]);
    }
}
