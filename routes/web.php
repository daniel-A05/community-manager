<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Use Illuminate\Support\Facades\Input;
Use App\Edital;
Use App\Agenda;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//INSERÇÃO/DELEÇÃO DE POSTS
Route::post('/home/add', 'FeedControle@create')->name('CriarFeed');
Route::get('/home/apagar/{id}', 'FeedControle@destroy')->name('ApagarFeed');

Route::resource('startup', 'StartupController');
//PALAVRA DO GESTOR
Route::get('/home/PalavraG', 'PalavraGController@store')->name('AdicionarP');
Route::get('/home/apaga/{id}', 'PalavraGController@destroy')->name('ApagarP');

Route::get('/Reset', function(){return view('auth.trocarSenha');})->name("nova");

Route::get('Comunidade', 'mentoresControle@index');

Route::get('/tabelaMentor/{id}', 'mentoresControle@show')->name('ativar');
Route::get('Mentores', function () {return view('Mentores');});

//INSERÇÃO DE DADOS PESSOAIS
Route::get('/Perfil', 'UserController@perfil')->name("perfil");
Route::post('/Perfil/dadosPessoais', 'UserController@updateFoto')->name("adc");

//Rotas para a página de editais
Route::get('/ShowEdital', 'EditalController@index')->name('show');
Route::get('/DetailEdital/{id}', 'EditalController@detailEdital');
Route::post('/ShowEdital', function(){
    $q = Input::get('search');
    if($q != ''){
        $edital = Edital::where('titulo', 'LIKE' ,'%'.$q.'%')->get();
        if(count($edital) >= 1 ){
            return view('edital.index', compact('edital'));
        }
    }
    return back();
});

//EDITAIS
Route::post('/Create', 'EditalController@inserir')->name('createEdital');
Route::post('/Update/{id}', 'EditalController@update')->name('updateEdital');
Route::get('/RemoveEdital/{id}', 'EditalController@remove');

//BLOG
Route::get('/Blog', 'BlogController@index')->name('Blog');
Route::post('Blog/add', 'BlogController@store')->name('criarBlog'); //criando dados do blog
Route::get('/deleteBlog/{id}', 'BlogController@destroy'); //ecluir dados do Blog
Route::get('/edita/{id}', 'BlogController@edit'); //direcionado para rota
Route::get('/editar/{id}', 'BlogController@update')->name("editar"); //editanoo os dados
Route::post('/Showblog', 'BlogController@show');

//AGENDA'
Route::get('/Agenda', 'AgendaController@index')->name('agendaShow');
Route::get('/AdicionarEvento', 'AgendaController@insert');
Route::post('/AtualizarEvento/{id}', 'AgendaController@update')->name('AtualizarEvento');
Route::get('/RemoverEvento/{id}', 'AgendaController@remove');
Route::post('/Agenda', function(){
    $q = Input::get('searchAgenda');
    if($q != ' '){
        $agenda = Agenda::where('titulo','LIKE' ,'%'.$q.'%')
                        ->orWhere('evento','LIKE' ,'%'.$q.'%')
                        ->orWhere('mentor','LIKE' ,'%'.$q.'%')
                        ->orWhere('local','LIKE' ,'%'.$q.'%')
                        ->orWhere('descricao','LIKE' ,'%'.$q.'%')
                        ->orWhere('equipamento','LIKE' ,'%'.$q.'%')->get();
        if(count($agenda) >= 1 ){
            return view('agenda.index', compact('agenda'));
        }
    }
    $agenda = Agenda::all();
    return view('agenda.index', compact('agenda'));
});
